# Production incident
We've observed that the ```executeStartupTask()``` function seems to freeze our micro service in the production environment.

Here's what was observed in the logs before the application froze and stopped processing.
```log
2022-02-09 08:56:32,874 INFO  method: [main] interview.InterviewApplication (InterviewApplication.java:115) - Updating model for device E2
2022-02-09 08:56:32,874 INFO  method: [Thread-2] interview.InterviewApplication (InterviewApplication.java:115) - Updating model for device E1
2022-02-09 08:56:32,877 INFO  method: [Thread-2] interview.InterviewApplication (InterviewApplication.java:50) - Sleep for 5000  milliseconds
2022-02-09 08:56:37,887 INFO  method: [Thread-2] interview.InterviewApplication (InterviewApplication.java:115) - Updating model for device E2
2022-02-09 08:56:37,889 INFO  method: [Thread-2] interview.InterviewApplication (InterviewApplication.java:56) - Sleep for 5000  milliseconds before commit
2022-02-09 08:56:42,894 INFO  method: [main] interview.InterviewApplication (InterviewApplication.java:92) - Sleep for 5000  milliseconds
2022-02-09 08:56:47,905 INFO  method: [main] interview.InterviewApplication (InterviewApplication.java:115) - Updating model for device E1
2022-02-09 08:56:47,908 INFO  method: [main] interview.InterviewApplication (InterviewApplication.java:97) - Sleep for 5000  milliseconds before commit
```

_FYI: When we change the value of ```generic-timeout-length``` for 0, the issue seems to occur randomly, but when the value is 5000, the issue always occurs._

We suspect an issue related to database transaction in the code. Could you identify a potential cause of the issue? Explain why the issue is occurring.