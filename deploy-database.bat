@echo off
echo "pulling mysql image"
docker pull mysql:5.6
echo "starting mysql server"
docker run --name interview-mysql -e MYSQL_ROOT_PASSWORD=Passw0rd! -p 127.0.0.1:3306:3306 -d mysql:5.6
timeout /t 3
echo "setting up mysql"
mysql --user root --password="Passw0rd!" -e "create database interviewtrilliant;"
mysql --user root --password="Passw0rd!" -e "use interviewtrilliant; create table device_entity (device_id varchar(10), provider_id bigint, model varchar(10));"
echo "waiting 3 seconds for operation to complete"
timeout /t 3
echo "done!"