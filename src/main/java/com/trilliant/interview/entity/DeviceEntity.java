package com.trilliant.interview.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "device_entity")
public class DeviceEntity {
  @Id
  @Column(name = "deviceId")
  private String deviceId;

  @Column(name = "providerId")
  private Long providerId;

  @Column(name = "model")
  private String model;
}
