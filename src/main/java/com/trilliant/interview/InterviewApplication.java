package com.trilliant.interview;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Arrays;

import javax.sql.DataSource;

import com.mysql.cj.jdbc.MysqlDataSource;
import com.trilliant.interview.entity.DeviceEntity;
import com.trilliant.interview.repository.DeviceRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;

@SpringBootApplication
public class InterviewApplication {

	@Autowired
	private DeviceRepository deviceRepository;

	private static Logger LOGGER = LogManager.getLogger(InterviewApplication.class);

	@Value(value = "${spring.datasource.url}")
	private String databaseUrl;

	@Value(value = "${generic-timeout-length}")
	private String genericTimeoutLength;

	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(InterviewApplication.class);
		app.run();
	}

	Thread thread = new Thread(() -> {
		Connection connection = null;
		try {
			connection = getMySQLDataSource().getConnection();
			connection.setAutoCommit(false);

			// 1st device model always GMETER
			updateDeviceModel("E1", "GMETER", connection);

			LOGGER.info("Sleep for " + genericTimeoutLength + "  milliseconds");
			Thread.sleep(Integer.parseInt(genericTimeoutLength));

			// 2st device model always EMETER
			updateDeviceModel("E2", "EMETER",  connection);

			LOGGER.info("Sleep for " + genericTimeoutLength + "  milliseconds before commit");
			Thread.sleep(Integer.parseInt(genericTimeoutLength));

			connection.commit();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			try {
				connection.close();
			}
			catch(SQLException throwables) {
				throwables.printStackTrace();
			}
		}
	});

	@EventListener(ApplicationReadyEvent.class)
	public void executeStartupTask() throws Exception {
		LOGGER.info("Connecting to MySQL via: " + databaseUrl);
		LOGGER.info("Seeding database...");
		seedDatabase();

		thread.start();

		// Resetting the model type

		Connection connection = null;

		try {
			connection = getMySQLDataSource().getConnection();
			connection.setAutoCommit(false);

			updateDeviceModel("E2", "NONE", connection);

			LOGGER.info("Sleep for " + genericTimeoutLength + "  milliseconds");
			Thread.sleep(Integer.parseInt(genericTimeoutLength));

			updateDeviceModel("E1", "NONE", connection);
			
			LOGGER.info("Sleep for " + genericTimeoutLength + "  milliseconds before commit");
			Thread.sleep(Integer.parseInt(genericTimeoutLength));

			connection.commit();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		finally {
			connection.close();
		}

		thread.join();

		LOGGER.info("EXECUTION SUCCESSFUL!");
	}

	public void updateDeviceModel(String deviceId, String model, Connection connection) throws SQLException {
		LOGGER.info("Updating model for device " + deviceId);
		PreparedStatement statement = connection.prepareStatement("UPDATE device_entity SET model = '" + model + "' WHERE device_id = '" + deviceId + "'");
		int updatedRow = statement.executeUpdate();

		if(updatedRow > 0) {
			return;
		}

		LOGGER.error("FAILED to update row for " + deviceId);
	}

	public DataSource getMySQLDataSource() {
		MysqlDataSource mysqlDS = null;
		mysqlDS = new MysqlDataSource();
		mysqlDS.setURL(databaseUrl);
		mysqlDS.setUser("root");
		mysqlDS.setPassword("Passw0rd!");
		return mysqlDS;
	}

	public void seedDatabase() {
		deviceRepository.deleteAll();

		DeviceEntity device1 = new DeviceEntity();
		device1.setDeviceId("E1");
		device1.setProviderId(99289L);
		device1.setModel("EMETER");

		deviceRepository.save(device1);

		DeviceEntity device2 = new DeviceEntity();
		device2.setDeviceId("E2");
		device2.setProviderId(99289L);
		device2.setModel("EMETER");

		deviceRepository.saveAll(Arrays.asList(device1, device2));
	}
}
